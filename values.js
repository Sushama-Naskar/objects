function values(obj) {
    let propertyValues = new Array();
    for (let property in obj) {
        propertyValues.push(obj[property]);
    }
    return propertyValues;

}
module.exports = values;