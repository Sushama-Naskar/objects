function keys(obj) {
    let propertyNames = new Array();
    for (let property in obj) {
        propertyNames.push(property);
    }
    return propertyNames ;
}
module.exports = keys;