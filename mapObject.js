function mapObject(obj, cb) {
    let newObject = {};
    for (let property in obj) {
        newObject[property] = cb(obj[property], property);
    }

    return newObject;

}
module.exports = mapObject;