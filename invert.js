function invert(obj) {
    let objectCopy = {};
    for (let property in obj) {
        objectCopy[obj[property]] = property;
    }
    return objectCopy;
}

module.exports = invert;