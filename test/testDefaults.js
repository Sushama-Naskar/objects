const testObject=require('../objectList/testObject.js');
const defaults = require('../defaults.js');

const defaultProps={name: 'Bruce Wayne', hobby:'painting'};
const newObject=defaults(testObject,defaultProps);
// console.log(newObject);

//check

const expectedResult={ name: 'Bruce Wayne', age: 36, location: 'Gotham', hobby: 'painting' };
const actualResult=newObject;
if(JSON.stringify(expectedResult)===JSON.stringify(actualResult)){
    console.log(actualResult);
}else{
    console.log('wrong');
}