const testObject=require('../objectList/testObject.js');
const invert= require('../invert.js');

const objectCopy=invert(testObject);
// console.log(objectCopy);

//check

const expectedResult={ '36': 'age', 'Bruce Wayne': 'name', 'Gotham': 'location' };
const actualResult=objectCopy;
if(JSON.stringify(expectedResult)===JSON.stringify(actualResult)){
    console.log(actualResult);
}else{
    console.log('wrong');
}