const testObject = require('../objectList/testObject.js');
const pairs = require('../pairs.js');

const arrayOfPairs = pairs(testObject);
// console.log(arrayOfPairs);

//check

const expectedResult = [['name', 'Bruce Wayne'], ['age', 36], ['location', 'Gotham']];
const actualResult = arrayOfPairs;
if (JSON.stringify(expectedResult) === JSON.stringify(actualResult)) {
    console.log(actualResult);
} else {
    console.log('wrong');
}