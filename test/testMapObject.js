const testObject = require('../objectList/testObject.js');
const mapObject = require('../mapObject.js');

function cb(value,key) {
    return value;
}
const newObject = mapObject(testObject, cb);
// console.log(newObject);

//check
const expectedResult = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const actualResult = newObject;
if (JSON.stringify(expectedResult) === JSON.stringify(actualResult)) {
    console.log(actualResult);
} else {
    console.log('wrong');
}
