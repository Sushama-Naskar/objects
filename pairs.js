function pairs(obj) {
    let arrayOfPairs = new Array();
    for (let property in obj) {
        arrayOfPairs.push([property, obj[property]]);
    }
    return arrayOfPairs;
}

module.exports = pairs;